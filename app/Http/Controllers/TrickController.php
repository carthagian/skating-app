<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTrickRequest;
use App\Http\Requests\FavouriteTrickRequest;
use App\Trick;
use App\User;
use Illuminate\Http\Request;

class TrickController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Trick::paginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateTrickRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTrickRequest $request)
    {
        $trick = Trick::create($request->all());

        return response()->json($trick);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param FavouriteTrickRequest $request
     * @param Trick $trick
     * @return void
     */
    public function favourite(FavouriteTrickRequest $request, Trick $trick)
    {


        $user = User::where('id', $request->user_id)->first();


        if ($request->favourite) {

            $user->favourites()->detach($trick->id);

            return response()->json(['status' => "Favourited trickId {$trick->id}"]);
        }

        $user->favourites()->detach($trick->id);

        return response()->json(['status' => "UnFavourited trickId {$trick->id}"]);

        //Fix it in the next version of the api??
        //Yes, this is a code smell
    }
}
