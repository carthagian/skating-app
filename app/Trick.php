<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trick extends Model
{
    protected $appends = ['full_name'];

    protected $fillable = [
        'user_id', 'name', 'description', 'location', 'video_link'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function getFullNameAttribute()
    {
        return $this->user->getFullName();
    }
}



