# Installation

    1. Clone repo
    2. run composer install
    3. Setup your .env configs
    4. run php artisan migrate to migrate the database
    
    
# Creating User
  @POST `/api/user`
  
  `{
  
  "first_name" : "",
  
  "last_name": "",
  
  "email": ""
  
  }`
  
  All the above are required fields
  
  
  
 #Tricks  
  
  1. ADD Trick
  
  @POST /api/trick
  
  {
    
  	"user_id" : "",
  	  	
  	"name" : "",
  	
  	"description" : "",
  	
  	"location": "",
  	
  	"video_link" : ""
  	
  }
  
  
  2. List Tricks
  
  @GET /api/trick
  
  
  3. Favourite Trick
  
  @POST /api/trick/{trick}/favourite