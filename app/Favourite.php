<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favourite extends Model
{
    protected $fillable = [
        'user_id', 'trick_id'
    ];


    public function user()
    {
        $this->belongsTo(User::class);
    }

    public function trick()
    {
        $this->belongsTo(Trick::class);
    }
}



