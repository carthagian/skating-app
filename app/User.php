<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class User extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function tricks()
    {
        return $this->hasMany(Trick::class);
    }

    public function getFullName()
    {
        return $this->first_name . " " . $this->last_name;
    }


    public function favourites()
    {
        return $this->belongsToMany(Favourite::class, 'favourites', 'user_id', 'id');
    }
}
